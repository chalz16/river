import re
import os
import datetime as dt
from datetime import timedelta
import string
from sys import argv
import logging

path_name='/tmp/'
try:
    fname = '{}/{}.riv'.format(path_name,argv[1])
except:
    fname= '/home/csimson/Downloads/2019021815.riv'
riverFilerRGX = re.compile("[0-9]+.riv")
riverFname = riverFilerRGX.findall(fname)
riverFileName = str(riverFname)
print(riverFileName)
riverFileDate = riverFileName.split('.')
newdict = {}
count = 0
hour = fname[(len(fname) - 6):(len(fname)- 4)]
line = ''
insideB = in_e = 0
tm = ''
pcode = ''
tz = ''
setOfTokens = []
innerdict = {}
noValue = False
dateValue = ()
TimeUnit = []
date1 = {}


def isinteger(a):
    try:
        int(a)
        return True
    except ValueError:
        return False
def remove_comments(s):
    return re.sub(r'(:[A-Za-z0-9 .,-]+:?)|:','',s) 
def getdatetime(t):

    y = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').year
    m = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').month
    d = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').day 
    try:
        if t[1] == 'H':      
            if(len(t) == 6):
                return y,m,d,int(t[2:4]),int(t[4:6]),0
            elif(len(t) == 8):
                return y,m,d,int(t[2:4]),int(t[4:6]),int(t[6:8])
            elif(len(t) == 4):
                return y,m,d,int(t[2:4]),0,0   
        elif t[1] == 'M':
            if(len(t) == 10):
                return y,m,d,int(t[6:8]),int(t[8:10]),0
            elif(len(t) == 12):
                return y,m,d,int(t[6:8]),int(t[8:10]),int(t[10:12])
            elif(len(t) == 8):
                return y,m,d,int(t[6:8]),0,0   
        elif t[1] == 'Y':
            if(len(t) == 10):
                return y,m,d,int(t[10:14]),int(t[14:16]),0
            elif(len(t) == 12):
                return y,m,d,int(t[10:14]),int(t[14:16]),int(t[16:18])
            elif(len(t) == 8):
                return y,m,d,int(t[10:14]),0,0  
    except IndexError:
        logging.warning('This is new %s',line)
        return y,m,d,0,0,0



class ShefDecoder:
    def __init__(self, contents):
        self.contents = contents
        self.count = 0

    def parseA(self):

        innerdict = {}
        formattedLine = remove_comments(line)
        setOfTokens = formattedLine.split()
        if len(setOfTokens) < 5:
            return
        tz = None
        if len(setOfTokens[3]) <= 2:
            tz = setOfTokens[3][0]
        else:
            tz = 'Z' 
        HGText = HGPatternA.findall(formattedLine)
        HGString = str(HGText[0])
        TimePart = HGPatternA.split(formattedLine)
        HGValueFormat = re.compile(r'[0-9]+[.]?[0-9]*')
        HGValueText = HGValueFormat.findall(HGString)
        if HGValueText == []:
            return
        try:
            HGValue = float(HGValueText[0])
        except ValueError:
            logging.warning('%s',line)
            return
        lineSplit = HGString.split()
        pcode = lineSplit[0][1:]
        TimePattern = re.compile('D[A-Za-z][0-9][0-9]+')
        TimeText = (TimePattern.findall(TimePart[0]))
        if len(TimeText):
            Time = TimeText[0][0:]
            if HGValue < 0.0:
                HGValue = 'M'
            time = getdatetime(Time)
            eldict = {'pcode':pcode, 'tzcode': tz, 'ob_tzcode': tz,'ob_time':time,'value':float(HGValue) }
            if setOfTokens[1] in newdict and newdict[setOfTokens[1]] == innerdict:
                newdict[setOfTokens[1]]['elements'].append(eldict)
            elif setOfTokens[1] in newdict and newdict[setOfTokens[1]] != innerdict:
                innerdict.update({})
            else:
                elements = [eldict]
                innerdict = {'revised':int(line[2] == 'R'),'rectype': setOfTokens[0][1], 'elements':elements}
                newdict.update({setOfTokens[1]:innerdict})
            self.count = self.count + 1
        else:
            logging.warning('%s',line)

    def parseB(self):
        revised = False
        global innerdict
        global insideB
        global tm
        global pcode
        global tz
        global setOfTokens
        global TimeUnit
        if line.startswith(".BR") and HGPatternB.search(line):
            revised = True
            formattedLine = remove_comments(line)
            formattedLine = formattedLine.replace('/', ' ')
            setOfTokens = formattedLine.split()
            if len(setOfTokens) > 7:
                return
            insideB = 1
            pcodeText = HGPatternB.findall(line)
            pcode = pcodeText[0][1:]
            insideB = 1
            pcodeText = HGPatternB.findall(line)
            pcode = pcodeText[0][0:]
            TimePattern = re.compile('D[A-Za-z][0-9][0-9]+')
            TimeText = (TimePattern.findall(setOfTokens[len(setOfTokens)-2])) 
            if len(setOfTokens[3]) <= 2:
                tz = setOfTokens[3][0]
            else:
                tz = 'Z'                
            if TimeText == []:
                tm = 'DH' + hour
            else:
                tm = TimeText[0][0:]
            return  
        elif line.startswith(".B") and HGPatternB.search(line):
            revised = False
            formattedLine = remove_comments(line)
            formattedLine = formattedLine.replace('/', ' ')
            setOfTokens = formattedLine.split()
            if len(setOfTokens) > 7:
                logging.warning('Too long %s',line) 
                return                   
            insideB = 1
            pcodeText = HGPatternB.findall(line)
            pcode = pcodeText[0][0:]
            TimePattern = re.compile('D[A-Za-z][0-9][0-9]+')
            TimeText = (TimePattern.findall(setOfTokens[len(setOfTokens)-2])) 
            if len(setOfTokens[3]) <= 2:
                tz = setOfTokens[3][0]
            else:
                tz = 'Z'                
            if TimeText == []:
                tm = 'DH' + hour
            else:
                tm = TimeText[0][0:]
            return   
        elif line.upper().startswith(".END"):
            insideB = 0
            revised = False
            return
        elif (insideB == 1) and not (line.startswith(':')):
            formattedLine = line.replace('/', ' ')
            formattedLine = remove_comments(formattedLine)
            lineSplit = formattedLine.split()
            if lineSplit == []:
                return
            HGValueFormat = re.compile(r'[0-9]+.[0-9]*')
            j = HGValueFormat.findall(lineSplit[len(lineSplit)-1])
            try:
                value = float(j[0])
            except IndexError:
                logging.warning('%s',line)
                return
            tm = lineSplit[1]
            time = getdatetime(tm)
            eldict = {'pcode':pcode, 'tzcode': tz, 'ob_tzcode': tz,'ob_time':time,'value':value }
            try:
                if lineSplit[0] in newdict and newdict[lineSplit[0]] == innerdict:
                    newdict[lineSplit[0]]['elements'].append(eldict)
                else:
                    elements = [eldict]
                    innerdict = {'revised':int(revised),'rectype': lineSplit[0][1], 'elements': elements}
                    newdict.update({lineSplit[0]:innerdict})
            except IndexError:
                logging.warning('%s',line)
                return
            self.count = self.count + 1
                
    def parseE(self):
        TimeText = []
        HGValueText = []
        global tz
        global pcode
        global noValue
        global setOfTokens
        global innerdict
        global dateValue
        global in_e
        global TimeUnit
        global date1
        formattedLine = line.replace('/', ' ')
        formattedLine = remove_comments(formattedLine)
        if line.startswith(".E") and line[2] != 'N':
            if (line.startswith(".E") or line.startswith(".ER")) and HGPatternE.search(formattedLine):
                if (not isinteger(line[2])) and (not isinteger(line[3])):
                    in_e = 1
                    noValue = False
                    setOfTokens = line.split()
                    pcodeText = HGPatternE.findall(formattedLine)
                    if len(pcodeText) == 1 and setOfTokens[1].startswith("HG"):
                        return
                    if len(pcodeText) == 1:
                        pcode = pcodeText[0]
                    else:
                        pcode = pcodeText[1]
                    lineSplit = HGPatternE.split(formattedLine[8:])
                    HGValueFormat = re.compile(r'[0-9]+[.][0-9]*')
                    HGValueText = HGValueFormat.findall(str(lineSplit[1:]))
                    try:
                        HGValue = float(HGValueText[0])
                    except IndexError:
                        noValue = True
                    if len(setOfTokens[3]) <= 2:
                        tz = setOfTokens[3][0]
                    else:
                        tz = 'Z'
                    TimePattern = re.compile(r'D[HMDY][0-9][0-9]+')
                    try:
                        TimeText = (TimePattern.findall(lineSplit[0]))
                    except Exception:
                        logging.warning('%s',lineSplit)
                        return                            
                    try:
                        tm = TimeText[0][0:]
                    except IndexError:
                        logging.warning('%s',line)
                        return
                    y = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').year
                    m = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').month
                    d = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').day 
                    if tm[1] == 'H': 
                        h = min = sec = 0     
                        if(len(tm) >= 4):
                            h = dt.datetime.strptime((tm[2:4]),'%H').hour
                            if(len(tm) >= 6):
                                min = dt.datetime.strptime((tm[4:6]),'%M').minute
                                if(len(tm) >= 8):
                                    sec = dt.datetime.strptime((tm[6:8]),'%S').second
                    elif tm[1] == 'M':
                        h = min = sec = 0
                        if(len(tm) >= 6):
                            h = dt.datetime.strptime((tm[4:6]),'%H').hour
                            if(len(tm) >= 8):
                                min = dt.datetime.strptime((tm[6:8]),'%M').minute
                                if(len(tm) >= 10):
                                    sec = dt.datetime.strptime((tm[8:10]),'%S').second
                    elif tm[1] == 'Y':
                        h = min = sec = 0
                        if(len(tm) >= 8):
                            h = dt.datetime.strptime((tm[8:10]),'%H').hour
                            if(len(tm) >= 10):
                                min = dt.datetime.strptime((tm[10:12]),'%M').minute
                                if(len(tm) >= 12):
                                    sec = dt.datetime.strptime((tm[12:14]),'%S').second
                    dateValue = (y,m,d,h,min,sec)
                    TimePattern2 = re.compile('D[A-Za-z][A-Za-z][-]?[0-9]+')
                    TimeUnit = TimePattern2.findall(formattedLine[10:])
                    date1 = dt.datetime(y,m,d,h,min,sec)                        
                    if noValue:
                        return
                    eldict = {'pcode':pcode, 'tzcode': tz, 'ob_tzcode': tz,'ob_time':dateValue,'value':HGValue }
                    elements = [eldict]
                    innerdict = {'revised':int((line[2] == 'R') or (line[3] == 'R')),'rectype': setOfTokens[0][1], 'elements':elements}
            elif in_e == 1 and ((line.startswith(".E") and isinteger(line[2])) or (line.startswith(".ER") and isinteger(line[3]))):
                HGValueFormat = re.compile(r'[0-9]+[.][0-9]*')
                HGValueText = HGValueFormat.findall(line)
            else:
                in_e = 0 
                return  
            for HGValue in HGValueText[0:]:
                eldict = {'pcode':pcode, 'tzcode': tz, 'ob_tzcode': tz,'ob_time':dateValue,'value':HGValue }
                if noValue:
                    elements = [eldict]
                    innerdict = {'revised':int((line[2] == 'R') or (line[3] == 'R')),'rectype': setOfTokens[0][1], 'elements':elements}                        
                    noValue = False
                else:
                    try:
                        newdict[setOfTokens[1]]['elements'].append(eldict)
                    except KeyError:
                        pass
                    except IndexError:
                        logging.warning('%s',line)
                try:
                    newdict.update({setOfTokens[1]:innerdict})
                except Exception as err:
                    logging.warning('Stopped Got to see this %s %s',line, err)
                    return
                self.count = self.count + 1
                TimeUnitPattern = re.compile('[-]?[0-9]+')
                try:
                    if str(TimeUnit[0][2]) == 'N':
                        TimeValue = TimeUnitPattern.findall(str(TimeUnit[0][3:]))
                        date1 = date1 + dt.timedelta(minutes = int(str(TimeValue[0])))
                    elif str(TimeUnit[0][2]) == 'H':
                        TimeValue = TimeUnitPattern.findall(str(TimeUnit[0][3:]))
                        date1 = date1 + dt.timedelta(hours = int(str(TimeValue[0])))
                    dateValue = (date1.year,date1.month,date1.day,date1.hour,date1.minute,date1.second)
                except IndexError:
                    logging.warning('%s',line)
                    return

                        
    def printCount(self):               
        print(self.count)



if __name__ == '__main__':
    with open(fname,'r',encoding = "Latin_1") as fp:
        lines=fp.readlines()
    x = ShefDecoder(lines)
    logging.basicConfig(filename='/home/csimson/Downloads/example.log',filemode='w',level=logging.DEBUG)
    HGPatternA = re.compile("/HG[A-Za-z0-9]* *[-]?[0-9]+[.]?[0-9]*")
    HGPatternB = re.compile(r'HG[A-Za-z0-9]*')
    HGPatternE = re.compile(r'\WHG[A-Za-z0-9]*')
    #with open (fname, "r", encoding = "Latin_1") as myfile:
    for line in lines:
        if line.startswith(".A") and HGPatternA.search(line):
            x.parseA()
        elif (line.startswith(".B") and HGPatternB.search(line)) or insideB == 1:
            x.parseB()
        elif (line.startswith(".E") and HGPatternB.search(line)) or in_e == 1:
            x.parseE()
    print(newdict)
    x.printCount()
