import re
import sys

def getdatetime(d,t):
    if(len(d) == 8):
        return int(d[0:4]),int(d[4:6]),int(d[6:8]),int(t[0:2]),int(t[2:3]),int(t[3:4])

def find_r(t):
    if t[2] == 'R':
        return 'R'
    else: 
        return '0'

class ShefDecoder:

    def __init__(self, contents):
        self.contents = contents

    def parseA(self):
        text = self.contents.splitlines()
        newdict = {}
        for s in text:
            if s.startswith(".A") and "HG" in s:
                a = s.split()
                print(a[3])
                tz = None 
                if len(a[3]) <= 2:
                    tz = a[3][0]
                    b = a[4].split('/')
                    value = a[5]
                    dt = a[3]
                else:
                    tz = 'Z' 
                    b = a[3].split('/') 
                    value = a[4]
                    dt = a[2]
                tm = b[0][2:]
                pcode = b[1]       
                newdict.update({a[1]:{'revised':find_r(s),'rectype': a[0][1], 'elements':({'pcode':pcode + tz + tz, 'tzcode': tz, 'ob_tzcode': tz,'ob_time':getdatetime(a[2],b[0][2:]),'value':float(value) },)}})
        return newdict

fname = sys.argv[1]
with open(fname,'r') as fp:
    shef_lines=fp.read()
x = ShefDecoder(shef_lines)
dvals=x.parseA()
print(dvals)
