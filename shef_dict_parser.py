import json
import statistics as s
import pytz
from datetime import datetime
from create_ris_test_orm import *
from shef_meta import shef_meta

utc = pytz.utc
east = pytz.timezone('US/Eastern')
cent = pytz.timezone('US/Central')
mntn = pytz.timezone('US/Mountain')

now = datetime.now()
yr = str(now.year)
mo = now.month
if mo <= 9:
    mo = '0' + str(now.month)
else:
    mo = str(now.month)
dy = now.day
if dy <= 9:
    dy = '0' + str(now.day)
else:
    dy = str(now.day)
hr = now.hour
if hr <= 9:
    hr = '0' + str(now.hour)
else:
    hr = str(now.hour)
dstr = yr + mo + dy + hr
print(dstr)

#Removed FMDI4, BLML1, BSRL1, GSCM7
sites = ['BRKI2', 'CIRI2', 'GCTI2', 'GDTI2', 'GLDI2', 'ILNI2', 'KHBI2', 'MOZI2',
    'NBOI2', 'QLDI2', 'RCKI2', 'UINI2', 'CLFI3', 'MTVI3', 'TELI3', 'BRLI4', 'DBQI4',
    'DLDI4', 'EOKI4', 'FAII4', 'GTTI4', 'LECI4', 'LNSI4', 'MUSI4', 'ATCK1',
    'BENK1', 'HKMK2', 'OWBK2', 'ALAL1', 'BBOL1', 'BCNL1', 'BCSL1',
    'BTRL1', 'CKBL1', 'DONL1', 'EMPL1', 'HLHL1', 'HPGL1', 'KLML1', 'KRZL1', 'MBBL1',
    'MBCL1', 'MCGL1', 'MLVL1', 'MPEL1', 'MRAL1', 'MRBL1', 'MWBL1', 'NOGL1', 'NORL1',
    'RMHL1', 'RRVL1', 'SLSL1', 'SMAL1', 'STFL1', 'SWBL1', 'VNCL1', 'WBOL1', 'WDSL1',
    'WPHL1', 'FTRM5', 'HSTM5', 'KNUM5', 'LCRM5', 'LKCM5', 'LSAM5', 'MSBM5', 'MSCM5',
    'MSVM5', 'RDWM5', 'SCLM5', 'SPAM5', 'SSPM5', 'WABM5', 'AEEM6', 'BIGM6', 'FLTM6',
    'FRPM6', 'LBLM6', 'MRTM6', 'TNFM6', 'BDPM7', 'BRIM7', 'CANM7', 'CLKM7', 'CMSM7',
    'COMM7', 'CRTM7', 'CTWM7', 'GGYM7', 'HAMM7', 'HRYM7', 'LGRM7', 'LUSM7',
    'MOCM7', 'NMDM7', 'PCLM7', 'RRLM7', 'SBEM7', 'STGM7', 'SVRM7', 'TMLM7', 'WVYM7',
    'CYNM8', 'OSWM8', 'RDVM8', 'BLAN1', 'MRPN1', 'PTMN1', 'GARN8', 'ELVO1', 'HNNO1',
    'POWO1', 'RNOO1', 'KAWO2', 'KERO2', 'KEYO2', 'MAYO2', 'PCYO2', 'WFLO2', 'GYVS2',
    'WSCS2', 'CHAT1', 'CHRT1', 'CKDT1', 'FLDT1', 'KGTT1', 'KNOT1', 'NJNT1', 'NKJT1',
    'PICT1', 'PRVT1', 'SAVT1', 'SPTT1', 'TPTT1', 'WBOT1', 'GALW2', 'RACW2', 'WELW2',
    'ALMW3', 'GENW3', 'LACW3', 'LAXW3', 'LYNW3', 'TREW3', 'DECA1', 'GVDA1', 'SCRA1',
    'WHLA1', 'WLSA1', 'ARSA4', 'HEEA4', 'MYLA4', 'OSGA4', 'OZGA4', 'VBUA4', 'YCNA4',
    'CDMC2', 'FWLC2', 'LXHC2', 'NPTC2', 'PRTC2', 'SALC2', 'WSVC2']

# #Used for mass loading by day
# for a in range(0, 24):
#     if a <= 9:
#         b = '0' + str(a)
#     else:
#         b = str(a)

    # print(b)

with open('/tmp/{}.json'.format(dstr,)) as f:
    data = json.load(f)

for site in sites:
    try: #Verify that site has data. If not, will skip to next site
        c = len(data[site])
    except:
        continue

    rev = []
    obtm = []
    vals = []

    for i in range(0, len(data[site])):
        if data[site][i]['elements'][0]['ob_time'][4] == 0 and data[site][i]['elements'][0]['pcode'] != ' HGIRZ':
            #print(data[site][i])
            rev.append(data[site][i]['revised'])
            vals.append(data[site][i]['elements'][0]['value'])

            raw_time_uf = data[site][i]['elements'][0]['ob_time']
            raw_tz = data[site][i]['elements'][0]['ob_tzcode']
            raw_time = datetime(raw_time_uf[0], raw_time_uf[1], raw_time_uf[2], raw_time_uf[3], raw_time_uf[4], raw_time_uf[5])

            if raw_tz == 'Z':
                raw_time = utc.localize(raw_time)
            elif raw_tz == 'E':
                raw_time = east.localize(raw_time)
            elif raw_tz == 'C':
                raw_time = cent.localize(raw_time)
            elif raw_tz == 'M':
                raw_time = mntn.localize(raw_time)
            else:
                continue

            utc_time = raw_time.astimezone(utc)

            ltz = shef_meta[site]['rtz']
            if ltz == 'America/New_York':
                local_time = utc_time.astimezone(east)
            elif ltz == 'America/Chicago':
                local_time = utc_time.astimezone(cent)
            elif ltz == 'America/Denver':
                local_time = utc_time.astimezone(mntn)
            else:
                continue

            dttm = local_time.strftime('%Y-%m-%d %H:%M:%S')
            obtm.append(dttm)
            #print(dttm)

    times = set(obtm)
    for time in times:
        idxs = [i for i, x in enumerate(obtm) if x == time]
        rdgs = [float(vals[idx]) for idx in idxs]
        avg = round(s.mean(rdgs), 2)
        #print(idxs, avg)

        if rev[idxs[0]] == 0:
            try:
                res = RiverData.insert(dttm = time, height = avg, usgs_id = site,
                    method = 'SHEF').execute()
            except Exception as e:
                print(e)
                continue
        #Updated sites are throwing bad values. Best to avoid.
