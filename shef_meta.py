shef_meta={
    "BRKI2": {
        "riv": "BRKI2",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CIRI2": {
        "riv": "CIRI2",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GCTI2": {
        "riv": "GCTI2",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GDTI2": {
        "riv": "GDTI2",
        "ap": "KMDH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GLDI2": {
        "riv": "GLDI2",
        "ap": "KBRL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "ILNI2": {
        "riv": "ILNI2",
        "ap": "KMLI",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "KHBI2": {
        "riv": "KHBI2",
        "ap": "KBRL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MOZI2": {
        "riv": "MOZI2",
        "ap": "KSTL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "NBOI2": {
        "riv": "NBOI2",
        "ap": "KBRL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "QLDI2": {
        "riv": "QLDI2",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "RCKI2": {
        "riv": "RCKI2",
        "ap": "KMLI",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "UINI2": {
        "riv": "UINI2",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CLFI3": {
        "riv": "CLFI3",
        "ap": "KLOU",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "MTVI3": {
        "riv": "MTVI3",
        "ap": "KEVV",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "TELI3": {
        "riv": "TELI3",
        "ap": "KEVV",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "BRLI4": {
        "riv": "BRLI4",
        "ap": "KBRL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "DBQI4": {
        "riv": "DBQI4",
        "ap": "KDBQ",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "DLDI4": {
        "riv": "DLDI4",
        "ap": "KDBQ",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "EOKI4": {
        "riv": "EOKI4",
        "ap": "KBRL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "FAII4": {
        "riv": "FAII4",
        "ap": "KMLI",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GTTI4": {
        "riv": "GTTI4",
        "ap": "KDBQ",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LECI4": {
        "riv": "LECI4",
        "ap": "KMLI",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LNSI4": {
        "riv": "LNSI4",
        "ap": "KLSE",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MUSI4": {
        "riv": "MUSI4",
        "ap": "KMLI4",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "ATCK1": {
        "riv": "ATCK1",
        "ap": "KSTJ",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "BENK1": {
        "riv": "BENK1",
        "ap": "KICT",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "HKMK2": {
        "riv": "HKMK2",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "OWBK2": {
        "riv": "OWBK2",
        "ap": "KOWB",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "ALAL1": {
        "riv": "ALAL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "BBOL1": {
        "riv": "BBOL1",
        "ap": "KP92",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "BCNL1": {
        "riv": "BCNL1",
        "ap": "KMSY",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "BCSL1": {
        "riv": "BCSL1",
        "ap": "KMSY",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "BTRL1": {
        "riv": "BTRL1",
        "ap": "KBTR",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CKBL1": {
        "riv": "CKBL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "DONL1": {
        "riv": "DONL1",
        "ap": "KBTR",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "EMPL1": {
        "riv": "EMPL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "HLHL1": {
        "riv": "HLHL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "HPGL1": {
        "riv": "HPGL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "KLML1": {
        "riv": "KLML1",
        "ap": "KBTR",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "KRZL1": {
        "riv": "KRZL1",
        "ap": "KLFT",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MBBL1": {
        "riv": "MBBL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MBCL1": {
        "riv": "MBCL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MCGL1": {
        "riv": "MCGL1",
        "ap": "KP92",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MLVL1": {
        "riv": "MLVL1",
        "ap": "KLFT",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MPEL1": {
        "riv": "MPEL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MRAL1": {
        "riv": "MRAL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MRBL1": {
        "riv": "MRBL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MWBL1": {
        "riv": "MWBL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "NOGL1": {
        "riv": "NOGL1",
        "ap": "KBTR",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "NORL1": {
        "riv": "NORL1",
        "ap": "KMSY",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "RMHL1": {
        "riv": "RMHL1",
        "ap": "KBTR",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "RRVL1": {
        "riv": "RRVL1",
        "ap": "KMSY",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SLSL1": {
        "riv": "SLSL1",
        "ap": "KBTR",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SMAL1": {
        "riv": "SMAL1",
        "ap": "KESF",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "STFL1": {
        "riv": "STFL1",
        "ap": "KBTR",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SWBL1": {
        "riv": "SWBL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "VNCL1": {
        "riv": "VNCL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WBOL1": {
        "riv": "WBOL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WDSL1": {
        "riv": "WDSL1",
        "ap": "KMSY",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WPHL1": {
        "riv": "WPHL1",
        "ap": "KNBG",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "FTRM5": {
        "riv": "FTRM5",
        "ap": "KBRD",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "HSTM5": {
        "riv": "HSTM5",
        "ap": "KMSP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "KNUM5": {
        "riv": "KNUM5",
        "ap": "KBRD",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LCRM5": {
        "riv": "LCRM5",
        "ap": "KLSE",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LKCM5": {
        "riv": "LKCM5",
        "ap": "KRST",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LSAM5": {
        "riv": "LSAM5",
        "ap": "KMSP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MSBM5": {
        "riv": "MSBM5",
        "ap": "KBRD",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MSCM5": {
        "riv": "MSCM5",
        "ap": "KLSE",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MSVM5": {
        "riv": "MSVM5",
        "ap": "KBRD",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "RDWM5": {
        "riv": "RDWM5",
        "ap": "KMSP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SCLM5": {
        "riv": "SCLM5",
        "ap": "KSTC",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SPAM5": {
        "riv": "SPAM5",
        "ap": "KMSP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SSPM5": {
        "riv": "SSPM5",
        "ap": "KMSP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WABM5": {
        "riv": "WABM5",
        "ap": "KRST",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "AEEM6": {
        "riv": "AEEM6",
        "ap": "KCBM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "BIGM6": {
        "riv": "BIGM6",
        "ap": "KTUP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "FLTM6": {
        "riv": "FLTM6",
        "ap": "KTUP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "FRPM6": {
        "riv": "FRPM6",
        "ap": "KMEM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LBLM6": {
        "riv": "LBLM6",
        "ap": "KTUP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MRTM6": {
        "riv": "MRTM6",
        "ap": "KTUP",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "BDPM7": {
        "riv": "BDPM7",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "BRIM7": {
        "riv": "BRIM7",
        "ap": "KSTL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CANM7": {
        "riv": "CANM7",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CLKM7": {
        "riv": "CLKM7",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CMSM7": {
        "riv": "CMSM7",
        "ap": "KJEF",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "COMM7": {
        "riv": "COMM7",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CRTM7": {
        "riv": "CRTM7",
        "ap": "KMKL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CTWM7": {
        "riv": "CTWM7",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GGYM7": {
        "riv": "GGYM7",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "HAMM7": {
        "riv": "HAMM7",
        "ap": "KOMA",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "HRYM7": {
        "riv": "HRYM7",
        "ap": "KJEF",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LGRM7": {
        "riv": "LGRM7",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LUSM7": {
        "riv": "LUSM7",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MOCM7": {
        "riv": "MOCM7",
        "ap": "KMDH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "NMDM7": {
        "riv": "NMDM7",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "PCLM7": {
        "riv": "PCLM7",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "RRLM7": {
        "riv": "RRLM7",
        "ap": "KMDH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SBEM7": {
        "riv": "SBEM7",
        "ap": "KMKC",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "STGM7": {
        "riv": "STGM7",
        "ap": "KSTL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SVRM7": {
        "riv": "SVRM7",
        "ap": "KUIN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "TMLM7": {
        "riv": "TMLM7",
        "ap": "KPAH",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WVYM7": {
        "riv": "WVYM7",
        "ap": "KMKC",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CYNM8": {
        "riv": "CYNM8",
        "ap": "KHLN",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "OSWM8": {
        "riv": "OSWM8",
        "ap": "KGGW",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "RDVM8": {
        "riv": "RDVM8",
        "ap": "KGGW",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "BLAN1": {
        "riv": "BLAN1",
        "ap": "KOMA",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MRPN1": {
        "riv": "MRPN1",
        "ap": "KSUX",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "PTMN1": {
        "riv": "PTMN1",
        "ap": "KOMA",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GARN8": {
        "riv": "GARN8",
        "ap": "KN60",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "ELVO1": {
        "riv": "ELVO1",
        "ap": "KPIT",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "HNNO1": {
        "riv": "HNNO1",
        "ap": "KPKB",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "POWO1": {
        "riv": "POWO1",
        "ap": "KCKB",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "RNOO1": {
        "riv": "RNOO1",
        "ap": "KPKB",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "KAWO2": {
        "riv": "KAWO2",
        "ap": "KPNC",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "KERO2": {
        "riv": "KERO2",
        "ap": "KFSM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "KEYO2": {
        "riv": "KEYO2",
        "ap": "KTUL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MAYO2": {
        "riv": "MAYO2",
        "ap": "KFSM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "PCYO2": {
        "riv": "PCYO2",
        "ap": "KPNC",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WFLO2": {
        "riv": "WFLO2",
        "ap": "KFSM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GYVS2": {
        "riv": "GYVS2",
        "ap": "KYKN",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WSCS2": {
        "riv": "WSCS2",
        "ap": "KABR",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CHAT1": {
        "riv": "CHAT1",
        "ap": "KCHA",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "CHRT1": {
        "riv": "CHRT1",
        "ap": "KCHA",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "CKDT1": {
        "riv": "CKDT1",
        "ap": "KCHA",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "FLDT1": {
        "riv": "FLDT1",
        "ap": "KTYS",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "KGTT1": {
        "riv": "KGTT1",
        "ap": "KTYS",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "KNOT1": {
        "riv": "KNOT1",
        "ap": "KTYS",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "NJNT1": {
        "riv": "NJNT1",
        "ap": "KMKL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "NKJT1": {
        "riv": "NKJT1",
        "ap": "KCHA",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "PICT1": {
        "riv": "PICT1",
        "ap": "KMSL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "PRVT1": {
        "riv": "PRVT1",
        "ap": "KMKL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SAVT1": {
        "riv": "SAVT1",
        "ap": "KMKL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SPTT1": {
        "riv": "SPTT1",
        "ap": "KCHA",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "TPTT1": {
        "riv": "TPTT1",
        "ap": "KMKL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WBOT1": {
        "riv": "WBOT1",
        "ap": "KCHA",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "GALW2": {
        "riv": "GALW2",
        "ap": "KHTS",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "RACW2": {
        "riv": "RACW2",
        "ap": "KCRW",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "WELW2": {
        "riv": "WELW2",
        "ap": "KPIT",
        "upstream_riv": [""],
        "rtz": "America/New_York"
    },
    "ALMW3": {
        "riv": "ALMW3",
        "ap": "KRST",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GENW3": {
        "riv": "GENW3",
        "ap": "KLSE",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LACW3": {
        "riv": "LACW3",
        "ap": "KLSE",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LAXW3": {
        "riv": "LAXW3",
        "ap": "KLSE",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "LYNW3": {
        "riv": "LYNW3",
        "ap": "KLSE",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "TREW3": {
        "riv": "TREW3",
        "ap": "KLSE",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "DECA1": {
        "riv": "DECA1",
        "ap": "KHSV",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "GVDA1": {
        "riv": "GVDA1",
        "ap": "KHSV",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "SCRA1": {
        "riv": "SCRA1",
        "ap": "KHSV",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WHLA1": {
        "riv": "WHLA1",
        "ap": "KMSL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "WLSA1": {
        "riv": "WLSA1",
        "ap": "KMSL",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "ARSA4": {
        "riv": "ARSA4",
        "ap": "KGWO",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "HEEA4": {
        "riv": "HEEA4",
        "ap": "KMEM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "MYLA4": {
        "riv": "MYLA4",
        "ap": "KLIT",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "OSGA4": {
        "riv": "OSGA4",
        "ap": "KMEM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "OZGA4": {
        "riv": "OZGA4",
        "ap": "KFSM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "VBUA4": {
        "riv": "VBUA4",
        "ap": "KFSM",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "YCNA4": {
        "riv": "YCNA4",
        "ap": "KGWO",
        "upstream_riv": [""],
        "rtz": "America/Chicago"
    },
    "CDMC2": {
        "riv": "CDMC2",
        "ap": "KPUB",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "FWLC2": {
        "riv": "FWLC2",
        "ap": "KPUB",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "LXHC2": {
        "riv": "LXHC2",
        "ap": "KPUB",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "NPTC2": {
        "riv": "NPTC2",
        "ap": "KPUB",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "PRTC2": {
        "riv": "PRTC2",
        "ap": "KPUB",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "SALC2": {
        "riv": "SALC2",
        "ap": "K04V",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
    "WSVC2": {
        "riv": "WSVC2",
        "ap": "K04V",
        "upstream_riv": [""],
        "rtz": "America/Denver"
    },
}
