from sqlalchemy.sql import select
from sqlalchemy import create_engine, Table, Column, Integer, BigInteger,Float,String, MetaData, ForeignKey, Text, DateTime
import string
import traceback

engine = create_engine('postgresql+psycopg2://storm2.lsu.edu:5432/ris', echo=True)
conn=engine.connect()
metadata = MetaData()

vrecs=Table('river_data', metadata,
        Column('usgs_id',Text,primary_key=True),
        Column('method',Text),
        Column('dttm',DateTime(timezone=False),primary_key=True),
        Column('height',Text),
)

#FOR GOD'S SAKE DO NOT DROP THE DATABASE ON STORM2!!!!!!!!!!!!
# try:
#         vrecs.drop(engine)
# except:
#         pass
vrecs.create(engine)
