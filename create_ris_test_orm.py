from peewee import *

database = PostgresqlDatabase('ris_test', **{'host': 'localhost', 'user': 'erohli'}, autorollback=True, autocommit=True)

class UnknownField(object):
    def __init__(self, *_, **__): pass

class BaseModel(Model):
    class Meta:
        database = database

class RiverData(BaseModel):
    dttm = DateTimeField(index=True)
    height = TextField(null=True)
    method = TextField(null=True)
    usgs = TextField(column_name='usgs_id', index=True)

    class Meta:
        table_name = 'river_data'
        indexes = (
            (('usgs', 'dttm'), True),
        )
        primary_key = CompositeKey('dttm', 'usgs')
