import plotly.plotly as py
import plotly.graph_objs as go
from plotly.graph_objs import *
import plotly.tools as tls
import psycopg2 as db
import pandas as pd
import sys

stn = str(sys.argv[1])

tls.set_credentials_file(username="erohli", api_key='X5aB7dvqH9SMOvE1je2j')
credentials = tls.get_credentials_file()

conn = db.connect('user=erohli dbname=ris_test host=localhost')
cur = conn.cursor()

cur.execute("select * from river_data where usgs_id=(%s) order by dttm asc;", (stn,))
rows = cur.fetchall()
df = pd.DataFrame(rows,columns=['usgs_id', 'method', 'dttm', 'height'])

dttm = df['dttm']
hght = df['height']

trace0 = go.Scatter(
    x = dttm,
    y = hght
)

data = [trace0]

py.plot(data, filename='{} Verification'.format(stn,), auto_open=True)
