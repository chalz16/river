import re
import os
import datetime as dt
from datetime import timedelta, datetime
import string
from sys import argv
import logging
import traceback
import json

now = datetime.now()
yr = str(now.year)
mo = now.month
if mo <= 9:
    mo = '0' + str(now.month)
else:
    mo = str(now.month)
dy = now.day
if dy <= 9:
    dy = '0' + str(now.day)
else:
    dy = str(now.day)
hr = now.hour
if hr <= 9:
    hr = '0' + str(now.hour)
else:
    hr = str(now.hour)
dstr = yr + mo + dy + hr
print(dstr)

path_name='/tmp/'
fname = '/ddplus/products/river/{}.riv'.format(dstr,)
# try:
#     fname = '{}/{}.riv'.format(path_name,argv[1])
# except:
#     fname= '/home/csimson/Downloads/2019030711.riv'
riverFilerRGX = re.compile("[0-9]+.riv")
riverFname = riverFilerRGX.findall(fname)
riverFileName = str(riverFname)
print(riverFileName)
riverFileDate = riverFileName.split('.')

def isinteger(a):
    try:
        int(a)
        return True
    except ValueError:
        return False
def remove_comments(s):
    return re.sub(r'(:[A-Za-z0-9 .,-]+:?)|:*','',s)
def getdatetime(tm):

    y = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').year
    m = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').month
    d = dt.datetime.strptime(riverFileDate[0][2:],'%Y%m%d%H').day
    try:
        h = min = sec = 0
        if tm[1] == 'H':
            if(len(tm) >= 4):
                try:
                    h = dt.datetime.strptime((tm[2:4]),'%H').hour
                except ValueError:
                    h = 0
                    d = d + 1
                if(len(tm) >= 6):
                    min = dt.datetime.strptime((tm[4:6]),'%M').minute
                    if(len(tm) >= 8):
                        sec = dt.datetime.strptime((tm[6:8]),'%S').second
        elif tm[1] == 'M':
            if(len(tm) >= 6):
                try:
                    h = dt.datetime.strptime((tm[2:4]),'%H').hour
                except ValueError:
                    h = 0
                    d = d + 1
                if(len(tm) >= 8):
                    min = dt.datetime.strptime((tm[6:8]),'%M').minute
                    if(len(tm) >= 10):
                        sec = dt.datetime.strptime((tm[8:10]),'%S').second
        elif tm[1] == 'Y':
            if(len(tm) >= 8):
                try:
                    h = dt.datetime.strptime((tm[2:4]),'%H').hour
                except ValueError:
                    h = 0
                    d = d + 1
                if(len(tm) >= 10):
                    min = dt.datetime.strptime((tm[10:12]),'%M').minute
                    if(len(tm) >= 12):
                        sec = dt.datetime.strptime((tm[12:14]),'%S').second
        return y,m,d,h,min,sec

    except IndexError:
        logging.warning('This is new %s',line)
        return y,m,d,0,0,0



class ShefDecoder:
    def __init__(self, EndFound = 0, insideB = 0, insideE = 0, dictABE = {}, Blist = [], Elist  = [], setOfTokens = [], HGValue = '', dateValue = '', tz = '', pcode = '', TimeUnit = '', date1 = {}):
        self.count = 0
        self.EndFound = EndFound
        self.insideB = insideB
        self.insideE = insideE
        self.dictABE = dictABE
        self.Blist = Blist
        self.Elist = Elist
        self.setOfTokens = setOfTokens
        self.HGValue = HGValue
        self.dateValue = dateValue
        self.tz = tz
        self.pcode = pcode
        self.date1 = date1
        self.TimeUnit = TimeUnit

    def parseA(self, line):
        innerdict = {}
        if line.startswith(".A") and HGPatternA.search(line):
            formattedLine = remove_comments(line)
            setOfTokens = formattedLine.split()
            if len(setOfTokens) < 5:
                return
            tz = None
            if len(setOfTokens[3]) <= 2:
                tz = setOfTokens[3][0]
            else:
                tz = 'Z'
            HGText = HGPatternA.findall(formattedLine)
            HGString = str(HGText[0])
            TimePart = HGPatternA.split(formattedLine)
            HGValueFormat = re.compile(r'-?[0-9]+[.]?[0-9]*')
            HGValueText = HGValueFormat.findall(HGString)
            if HGValueText == []:
                return
            try:
                HGValue = float(HGValueText[0])
            except ValueError:
                logging.warning('A error %s',line)
                return
            lineSplit = HGString.split()
            pcode = lineSplit[0][1:]
            TimePattern = re.compile('D[A-Za-z][0-9][0-9]+')
            TimeText = (TimePattern.findall(TimePart[0]))
            if len(TimeText):
                Time = TimeText[0][0:]
                HGValue = float(HGValue)
                if HGValue < -99.0:
                    HGValue = 'M'
                elif HGValue < 0:
                    return
                time = getdatetime(Time)
                eldict = {'pcode':pcode, 'tzcode': tz, 'ob_tzcode': tz,'ob_time':time,'value':HGValue }
                innerdict = {'revised':int(line[2] == 'R'),'rectype': setOfTokens[0][1], 'elements':[eldict]}
                if setOfTokens[1] not in self.dictABE:
                    self.dictABE.update({setOfTokens[1]:[innerdict]})
                    self.count = self.count + 1
                elif self.dictABE[setOfTokens[1]] != innerdict:
                    self.dictABE[setOfTokens[1]].append(innerdict)
                    self.count = self.count + 1
            else:
                logging.warning('A error %s',line)

    def parseB(self, line):
        pcodeText = []
        #hour = fname[(len(fname) - 6):(len(fname)- 4)]

        if (line.startswith(".END") or line.startswith(".End")) and self.insideB == 1:
            self.insideB = 0
            try:
                formattedLine = remove_comments(self.Blist[0])
                formattedLine = formattedLine.replace('/', ' ')
                pcodeText = HGPatternB.findall(self.Blist[0])
                pcode = pcodeText[0][0:]
                setOfTokens = formattedLine.split()
            except IndexError:
                logging.warning("B error at %s", self.Blist)
                return
            if setOfTokens[3].startswith("D"):
                time = getdatetime(setOfTokens[3])
            if setOfTokens[4].startswith("D"):
                time = getdatetime(setOfTokens[4])
            for lineB in self.Blist[1:]:
                formattedLine = remove_comments(lineB)
                formattedLine = formattedLine.replace('/', ' ')
                lineSplit = formattedLine.split()
                if lineSplit == []:
                    continue
                HGValueFormat = re.compile(r'-?[0-9]+.[0-9]*')
                j = HGValueFormat.findall(lineSplit[len(lineSplit)-1])
                try:
                    value = float(j[0])
                except:
                    logging.warning('%s',lineB)
                    continue
                if value < 0:
                    continue
                TimePattern = re.compile('D[A-Za-z][0-9][0-9]+')
                TimeText = (TimePattern.findall(lineSplit[len(lineSplit)-2]))
                if len(setOfTokens[3]) <= 2:
                    tz = setOfTokens[3][0]
                else:
                    tz = 'Z'
                if TimeText != []:
                    tm = TimeText[0][0:]
                    time = getdatetime(tm)
                eldict = {'pcode':pcode, 'tzcode': tz, 'ob_tzcode': tz,'ob_time':time,'value':value }
                elements = [eldict]
                innerdict = {'revised':int(setOfTokens[0].endswith('R')),'rectype': setOfTokens[0][1], 'elements': elements}
                try:
                    if lineSplit[0] not in self.dictABE:
                        self.dictABE.update({lineSplit[0]:[innerdict]})
                        self.count = self.count + 1
                    elif self.dictABE[lineSplit[0]] != innerdict:
                        self.dictABE[lineSplit[0]].append(innerdict)
                        self.count = self.count + 1
                    else:
                        print('is this possible')
                except IndexError:
                    logging.warning('%s',self.Blist)
                    continue
                #print("%d - %s", self.count, lineB)
            self.insideB = 0
            self.Blist = []
            return
        if (line.startswith(".B") and HGPatternB.search(line)) or self.insideB == 1:
            temp = line
            temp = remove_comments(temp)
            temp = temp.replace('/', ' ')
            s = temp.split()
            if len(s) > 7:
                return
            self.insideB = 1
            self.Blist.append(line)

    def parseE(self,line):
        HGValueText = []
        HGPatternC = re.compile(r'\SHG[A-Za-z0-9]*')
        formattedLine = line.replace('/', ' ')
        formattedLine = remove_comments(formattedLine)
        if self.insideE == 1 and formattedLine.startswith(".") and not (isinteger(formattedLine[2]) or isinteger(formattedLine[3])):
            self.insideE = 0
            self.Elist = []
        if (formattedLine.startswith(".E") and
           formattedLine[2] != 'N' and
           not isinteger(formattedLine[2]) and
           not isinteger(formattedLine[3]) and
           HGPatternC.findall(line) and
           self.insideE == 0):
            self.Elist.append(formattedLine)
            self.insideE = 1
            self.noValue = False
            self.setOfTokens = line.split()
            HGPatternCDouble = re.compile(r'\sHG[A-Za-z0-9]*')
            pcodeText = HGPatternCDouble.findall(formattedLine)
            if len(pcodeText) == 1:
                self.pcode = pcodeText[0][1:]
            elif len(pcodeText) > 1:
                self.pcode = pcodeText[1][1:]
            else:
                logging.warning('pcode error %s',line)
                return
            lineSplit = HGPatternCDouble.split(formattedLine[8:])
            HGValueFormat = re.compile(r'-?[0-9]+[.][0-9]*')
            HGValueText = HGValueFormat.findall(str(lineSplit[1:]))
            try:
                self.HGValue = float(HGValueText[0])
            except IndexError:
                self.noValue = True
            if len(self.setOfTokens[3]) <= 2:
                self.tz = self.setOfTokens[3][0]
            else:
                self.tz = 'Z'
            TimePattern = re.compile(r'D[HMDY][0-9][0-9]+')
            try:
                TimeText = (TimePattern.findall(lineSplit[0]))
            except Exception:
                logging.warning('%s',lineSplit)
                return
            try:
                tm = TimeText[0][0:]
            except IndexError:
                logging.warning('%s',line)
                return
            self.dateValue = getdatetime(tm)
            TimePattern2 = re.compile('D[A-Za-z][A-Za-z][-]?[0-9]+')
            self.TimeUnit = TimePattern2.findall(formattedLine[10:])
            self.date1 = dt.datetime(self.dateValue[0],self.dateValue[1],self.dateValue[2],self.dateValue[3],self.dateValue[4],self.dateValue[5])
            if self.noValue == False:
                eldict = {'pcode':self.pcode, 'tzcode': self.tz, 'ob_tzcode': self.tz,'ob_time':self.dateValue,'value':self.HGValue }
                elements = [eldict]
                innerdict = {'revised':int((line[2] == 'R') or (line[3] == 'R')),'rectype': self.setOfTokens[0][1], 'elements':elements}

        elif (formattedLine.startswith(".E") and (isinteger(formattedLine[2]) or isinteger(formattedLine[3])) and self.insideE == 1):
            self.Elist.append(formattedLine)
            HGValueFormat = re.compile(r'-?[0-9]+[.][0-9]*')
            HGValueText = HGValueFormat.findall(line)
        for self.HGValue in HGValueText[0:]:
            if float(self.HGValue) < 0:
                continue
            eldict = {'pcode':self.pcode, 'tzcode': self.tz, 'ob_tzcode': self.tz,'ob_time':self.dateValue,'value':self.HGValue }
            try:
                innerdict = {'revised':int((line[2] == 'R') or (line[3] == 'R')),'rectype': self.setOfTokens[0][1], 'elements':[eldict]}
            except IndexError:
                logging.warning('%s', line)
            if self.setOfTokens[1] not in self.dictABE:
                self.dictABE.update({self.setOfTokens[1]:[innerdict]})
                self.count = self.count + 1
            elif self.dictABE[self.setOfTokens[1]] != innerdict:
                self.dictABE[self.setOfTokens[1]].append(innerdict)
                self.count = self.count + 1
            TimeUnitPattern = re.compile('[-]?[0-9]+')
            try:
                if str(self.TimeUnit[0][2]) == 'N':
                    TimeValue = TimeUnitPattern.findall(str(self.TimeUnit[0][3:]))
                    self.date1 = self.date1 + dt.timedelta(minutes = int(str(TimeValue[0])))
                elif str(self.TimeUnit[0][2]) == 'H':
                    TimeValue = TimeUnitPattern.findall(str(self.TimeUnit[0][3:]))
                    self.date1 = self.date1 + dt.timedelta(hours = int(str(TimeValue[0])))
                self.dateValue = (self.date1.year,self.date1.month,self.date1.day,self.date1.hour,self.date1.minute,self.date1.second)
            except IndexError:
                logging.warning('%s',line)
                return

    def printCount(self):
        print(self.count)

    def printDict(self):
        print(self.dictABE)

    def dumpDict(self):
        with open("/tmp/{}.json", "w") as f:
            json.dump(self.dictABE, f)

if __name__ == '__main__':
    with open(fname,'r',encoding = "Latin_1") as fp:
        lines=fp.readlines()
    x = ShefDecoder()
    logging.basicConfig(filename='/tmp/example.log',filemode='w',level=logging.DEBUG)
    HGPatternA = re.compile(r"[/| ]HG[A-Za-z0-9]* *[-]?[0-9]+[.]?[0-9]*")
    HGPatternB = re.compile(r'HG[A-Za-z0-9]*')
    HGPatternE = re.compile(r'\WHG[A-Za-z0-9]*')
    for line in lines:
        if line.startswith(".") or line[0].isalpha():
            x.parseA(line)
            x.parseB(line)
            x.parseE(line)
    #x.printDict()
    x.printCount()
    x.dumpDict()
